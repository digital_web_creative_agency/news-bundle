<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluArticleBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluArticleBundle\Entity\Factory;

use Sulu\Bundle\MediaBundle\Entity\Media;

interface MediaFactoryInterface
{
    public function generateMedia($header): ?Media;
}
