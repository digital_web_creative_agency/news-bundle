<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluArticleBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluArticleBundle\Entity\Factory;

use Sulu\Bundle\ContactBundle\Entity\ContactRepositoryInterface;
use Sulu\Component\Persistence\RelationTrait;
use DigitalWeb\Bundle\SuluArticleBundle\Entity\Article;

class ArticleFactory extends AbstractFactory implements ArticleFactoryInterface
{
    use RelationTrait;

    private MediaFactoryInterface $mediaFactory;

    private TagFactoryInterface $tagFactory;

    private ContactRepositoryInterface $contactRepository;

    /**
     * ArticleFactory constructor.
     */
    public function __construct(
        MediaFactoryInterface $mediaFactory,
        TagFactoryInterface $tagFactory,
        ContactRepositoryInterface $contactRepository
    ) {
        $this->mediaFactory = $mediaFactory;
        $this->tagFactory = $tagFactory;
        $this->contactRepository = $contactRepository;
    }

    /**
     * @param null|mixed $state
     *
     * @throws \Exception
     */
    public function generateArticleFromRequest(Article $article, array $data, string $locale = null, $state = null): Article
    {
        if ($this->getProperty($data, 'title')) {
            $article->setTitle($this->getProperty($data, 'title'));
        }

        if ($this->getProperty($data, 'teaser')) {
            $article->setTeaser($this->getProperty($data, 'teaser'));
        }

        if ($this->getProperty($data, 'header')) {
            $article->setHeader($this->mediaFactory->generateMedia($data['header']));
        }

        if ($this->getProperty($data, 'publishedAt')) {
            $article->setPublishedAt(new \DateTime($this->getProperty($data, 'publishedAt')));
        }

        if ($this->getProperty($data, 'content')) {
            $article->setContent($this->getProperty($data, 'content'));
        }

        if ($this->getProperty($data, 'ext')) {
            $article->setSeo($this->getProperty($data['ext'], 'seo'));
        }

        if ($tags = $this->getProperty($data, 'tags')) {
            $this->tagFactory->processTags($article, $tags);
        }

        if (!$article->getId()) {
            $article->setCreated(new \DateTime());
        }

        if ($locale) {
            $article->setLocale($locale);
        }

        if (null !== $state) {
            $article->setEnabled($state);
        }

        if ($authored = $this->getProperty($data, 'authored')) {
            $article->setCreated(new \DateTime($authored));
        }

        if ($author = $this->getProperty($data, 'author')) {
            // @var Contact $contact
            $article->setCreator($this->contactRepository->find($author));
        }

        return $article;
    }
}
