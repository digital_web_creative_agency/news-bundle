<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluArticleBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluArticleBundle\Entity\Factory;

use DigitalWeb\Bundle\SuluArticleBundle\Entity\Article;

interface ArticleFactoryInterface
{
    public function generateArticleFromRequest(Article $article, array $data, string $locale): Article;
}
