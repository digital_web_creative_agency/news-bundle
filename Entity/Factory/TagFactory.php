<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluArticleBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluArticleBundle\Entity\Factory;

use Sulu\Bundle\TagBundle\Tag\TagManagerInterface;
use Sulu\Component\Persistence\RelationTrait;
use DigitalWeb\Bundle\SuluArticleBundle\Entity\Article;

class TagFactory extends AbstractFactory implements TagFactoryInterface
{
    use RelationTrait;
    private TagManagerInterface $tagManager;

    /**
     * TagFactory constructor.
     */
    public function __construct(TagManagerInterface $tagManager)
    {
        $this->tagManager = $tagManager;
    }

    /**
     * @return bool
     */
    public function processTags(Article $article, $tags)
    {
        $get = function ($tag) {
            return $tag->getId();
        };

        $delete = function ($tag) use ($article) {
            return $article->removeTag($tag);
        };

        $update = function () {
            return true;
        };

        $add = function ($tag) use ($article) {
            return $this->addTag($article, $tag);
        };

        $entities = $article->getTags();

        return $this->processSubEntities(
            $entities,
            $tags,
            $get,
            $add,
            $update,
            $delete
        );
    }

    /**
     * Returns the tag manager.
     *
     * @return TagManagerInterface
     */
    public function getTagManager()
    {
        return $this->tagManager;
    }

    /**
     * Adds a new tag to the given contact and persist it with the given object manager.
     *
     * @return bool True if there was no error, otherwise false
     */
    protected function addTag(Article $article, $data)
    {
        $success = true;
        $resolvedTag = $this->getTagManager()->findOrCreateByName($data);
        $article->addTag($resolvedTag);

        return $success;
    }
}
