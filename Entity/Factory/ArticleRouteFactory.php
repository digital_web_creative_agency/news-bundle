<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluArticleBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluArticleBundle\Entity\Factory;

use Sulu\Bundle\RouteBundle\Manager\RouteManager;
use Sulu\Bundle\RouteBundle\Model\RouteInterface;
use DigitalWeb\Bundle\SuluArticleBundle\Entity\Article;

class ArticleRouteFactory implements ArticleRouteFactoryInterface
{
    private RouteManager $routeManager;

    /**
     * ArticleFactory constructor.
     */
    public function __construct(
        RouteManager $manager
    ) {
        $this->routeManager = $manager;
    }

    public function generateArticleRoute(Article $article): RouteInterface
    {
        return $this->routeManager->create($article);
    }

    public function updateArticleRoute(Article $article, string $routePath): RouteInterface
    {
        return $this->routeManager->update($article, $routePath);
    }
}
