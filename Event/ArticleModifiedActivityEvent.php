<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluArticleBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluArticleBundle\Event;

use Sulu\Bundle\ActivityBundle\Domain\Event\DomainEvent;
use DigitalWeb\Bundle\SuluArticleBundle\Admin\ArticleAdmin;
use DigitalWeb\Bundle\SuluArticleBundle\Entity\Article;

class ArticleModifiedActivityEvent extends DomainEvent
{
    private Article $article;

    private array $payload;

    public function __construct(
        Article $book,
        array $payload
    ) {
        parent::__construct();

        $this->article = $book;
        $this->payload = $payload;
    }

    public function getEventType(): string
    {
        return 'modified';
    }

    public function getResourceKey(): string
    {
        return Article::RESOURCE_KEY;
    }

    public function getResourceId(): string
    {
        return (string) $this->article->getId();
    }

    public function getEventPayload(): ?array
    {
        return $this->payload;
    }

    public function getResourceTitle(): ?string
    {
        return $this->article->getTitle();
    }

    public function getResourceSecurityContext(): ?string
    {
        return ArticleAdmin::SECURITY_CONTEXT;
    }
}
