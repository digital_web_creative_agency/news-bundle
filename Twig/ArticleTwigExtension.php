<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluArticleBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluArticleBundle\Twig;

use Doctrine\Common\Cache\Cache;
use DigitalWeb\Bundle\SuluArticleBundle\Entity\Article;
use DigitalWeb\Bundle\SuluArticleBundle\Repository\ArticleRepository;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Extension to handle article in frontend.
 */
class ArticleTwigExtension extends AbstractExtension
{
    public function __construct(
        private readonly Cache $cache,
        private readonly ArticleRepository $articleRepository
    ) {
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('sulu_resolve_article', [$this, 'resolveArticleFunction']),
        ];
    }

    public function resolveArticleFunction(int $id): ?Article
    {
        if ($this->cache->contains($id)) {
            return $this->cache->fetch($id);
        }

        $article = $this->articleRepository->find($id);
        if (null === $article) {
            return null;
        }

        $this->cache->save($id, $article);

        return $article;
    }
}
