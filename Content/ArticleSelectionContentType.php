<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluArticleBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluArticleBundle\Content;

use Sulu\Component\Content\Compat\PropertyInterface;
use Sulu\Component\Content\SimpleContentType;
use DigitalWeb\Bundle\SuluArticleBundle\Entity\Article;
use DigitalWeb\Bundle\SuluArticleBundle\Repository\ArticleRepository;

class ArticleSelectionContentType extends SimpleContentType
{
    /**
     * @var ArticleRepository
     */
    private $articleRepository;

    public function __construct(ArticleRepository $articleRepository)
    {
        parent::__construct('article_selection', []);

        $this->articleRepository = $articleRepository;
    }

    /**
     * @return Article[]
     */
    public function getContentData(PropertyInterface $property): array
    {
        $ids = $property->getValue();

        $article = [];
        foreach ($ids ?: [] as $id) {
            $singleArticle = $this->articleRepository->findById((int) $id);
            if ($singleArticle) {
                $article[] = $singleArticle;
            }
        }

        return $article;
    }

    /**
     * {@inheritdoc}
     */
    public function getViewData(PropertyInterface $property)
    {
        return [
            'ids' => $property->getValue(),
        ];
    }
}
