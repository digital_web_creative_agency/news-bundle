<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluArticleBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluArticleBundle\Content;

use JMS\Serializer\Annotation as Serializer;
use Sulu\Component\SmartContent\ItemInterface;
use DigitalWeb\Bundle\SuluArticleBundle\Entity\Article;

class ArticleDataItem implements ItemInterface
{
    /**
     * @var Article
     *
     * @Serializer\Exclude
     */
    private $entity;

    /**
     * ArticleDataItem constructor.
     */
    public function __construct(Article $entity)
    {
        $this->entity = $entity;
    }

    /**
     * @Serializer\VirtualProperty
     */
    public function getId()
    {
        return $this->entity->getId();
    }

    /**
     * @Serializer\VirtualProperty
     */
    public function getTitle()
    {
        return $this->entity->getTitle();
    }

    /**
     * @Serializer\VirtualProperty
     */
    public function getImage()
    {
        return $this->entity->getHeader();
    }

    /**
     * @return mixed|Article
     */
    public function getResource()
    {
        return $this->entity;
    }
}
