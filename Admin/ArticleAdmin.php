<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluArticleBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluArticleBundle\Admin;

use Sulu\Bundle\ActivityBundle\Infrastructure\Sulu\Admin\View\ActivityViewBuilderFactoryInterface;
use Sulu\Bundle\AdminBundle\Admin\Admin;
use Sulu\Bundle\AdminBundle\Admin\Navigation\NavigationItem;
use Sulu\Bundle\AdminBundle\Admin\Navigation\NavigationItemCollection;
use Sulu\Bundle\AdminBundle\Admin\View\DropdownToolbarAction;
use Sulu\Bundle\AdminBundle\Admin\View\ToolbarAction;
use Sulu\Bundle\AdminBundle\Admin\View\ViewBuilderFactoryInterface;
use Sulu\Bundle\AdminBundle\Admin\View\ViewCollection;
use Sulu\Component\Security\Authorization\PermissionTypes;
use Sulu\Component\Security\Authorization\SecurityCheckerInterface;
use Sulu\Component\Webspace\Manager\WebspaceManagerInterface;
use DigitalWeb\Bundle\SuluArticleBundle\Entity\Article;

class ArticleAdmin extends Admin
{
    public const SECURITY_CONTEXT = 'sulu.article';

    public const NEWS_LIST_KEY = 'article';

    public const NEWS_FORM_KEY_ADD = 'article_details_add';

    public const NEWS_FORM_KEY_EDIT = 'article_details_edit';

    public const NEWS_LIST_VIEW = 'app.article_list';

    public const NEWS_ADD_FORM_VIEW = 'app.article_add_form';

    public const NEWS_EDIT_FORM_VIEW = 'app.article_edit_form';

    public const NEWS_FORM_KEY_SETTINGS = 'article_settings';

    /**
     * @var ViewBuilderFactoryInterface
     */
    private $viewBuilderFactory;

    /**
     * @var WebspaceManagerInterface
     */
    private $articleManager;

    /**
     * @var SecurityCheckerInterface
     */
    private $securityChecker;

    /**
     * @var ActivityViewBuilderFactoryInterface
     */
    private $activityViewBuilderFactory;

    /**
     * ArticleAdmin constructor.
     */
    public function __construct(
        ViewBuilderFactoryInterface $viewBuilderFactory,
        WebspaceManagerInterface $articleManager,
        SecurityCheckerInterface $securityChecker,
        ActivityViewBuilderFactoryInterface $activityViewBuilderFactory,
    ) {
        $this->viewBuilderFactory = $viewBuilderFactory;
        $this->articleManager = $articleManager;
        $this->securityChecker = $securityChecker;
        $this->activityViewBuilderFactory = $activityViewBuilderFactory;
    }

    public function configureNavigationItems(NavigationItemCollection $navigationItemCollection): void
    {
        if ($this->securityChecker->hasPermission(static::SECURITY_CONTEXT, PermissionTypes::VIEW)) {
            $module = new NavigationItem('sulu.article');
            $module->setPosition(20);
            $module->setIcon('su-news');
            $module->setView(static::NEWS_LIST_VIEW);

            $navigationItemCollection->add($module);
        }
    }

    public function configureViews(ViewCollection $viewCollection): void
    {
        $locales = $this->articleManager->getAllLocales();

        // Configure article List View
        $listToolbarActions = [new ToolbarAction('sulu_admin.add'), new ToolbarAction('sulu_admin.delete')];
        $listView = $this->viewBuilderFactory->createListViewBuilder(self::NEWS_LIST_VIEW, '/article/:locale')
            ->setResourceKey(Article::RESOURCE_KEY)
            ->setListKey(self::NEWS_LIST_KEY)
            ->setTitle('sulu.article')
            ->addListAdapters(['table'])
            ->addLocales($locales)
            ->setDefaultLocale($locales[0])
            ->setAddView(static::NEWS_ADD_FORM_VIEW)
            ->setEditView(static::NEWS_EDIT_FORM_VIEW)
            ->addToolbarActions($listToolbarActions)
        ;
        $viewCollection->add($listView);

        $addFormView = $this->viewBuilderFactory->createResourceTabViewBuilder(self::NEWS_ADD_FORM_VIEW, '/article/:locale/add')
            ->setResourceKey(Article::RESOURCE_KEY)
            ->setBackView(static::NEWS_LIST_VIEW)
            ->addLocales($locales)
        ;
        $viewCollection->add($addFormView);

        $addDetailsFormView = $this->viewBuilderFactory->createFormViewBuilder(self::NEWS_ADD_FORM_VIEW.'.details', '/details')
            ->setResourceKey(Article::RESOURCE_KEY)
            ->setFormKey(self::NEWS_FORM_KEY_ADD)
            ->setTabTitle('sulu_admin.details')
            ->setEditView(static::NEWS_EDIT_FORM_VIEW)
            ->addToolbarActions([new ToolbarAction('sulu_admin.save')])
            ->setParent(static::NEWS_ADD_FORM_VIEW)
        ;
        $viewCollection->add($addDetailsFormView);

        // Configure article Edit View
        $editFormView = $this->viewBuilderFactory->createResourceTabViewBuilder(static::NEWS_EDIT_FORM_VIEW, '/article/:locale/:id')
            ->setResourceKey(Article::RESOURCE_KEY)
            ->setBackView(static::NEWS_LIST_VIEW)
            ->setTitleProperty('title')
            ->addLocales($locales)
        ;
        $viewCollection->add($editFormView);

        $formToolbarActions = [];

        if ($this->securityChecker->hasPermission(static::SECURITY_CONTEXT, PermissionTypes::EDIT)) {
            $formToolbarActions[] = new DropdownToolbarAction(
                'sulu_admin.save',
                'su-save',
                [
                    new ToolbarAction(
                        'sulu_admin.save',
                        [
                            'label' => 'sulu_admin.save_draft',
                            'options' => ['action' => 'draft'],
                        ]
                    ),
                    new ToolbarAction(
                        'sulu_admin.save',
                        [
                            'label' => 'sulu_admin.save_publish',
                            'options' => ['action' => 'publish'],
                        ]
                    ),
                    new ToolbarAction(
                        'sulu_admin.publish',
                    ),
                ]
            );
        }

        if ($this->securityChecker->hasPermission(static::SECURITY_CONTEXT, PermissionTypes::DELETE)) {
            $formToolbarActions[] = new ToolbarAction('sulu_admin.delete');
        }

        if ($this->securityChecker->hasPermission(static::SECURITY_CONTEXT, PermissionTypes::LIVE)) {
            $formToolbarActions[] = new DropdownToolbarAction(
                'sulu_admin.edit',
                'su-pen',
                [
                    new ToolbarAction(
                        'sulu_admin.delete_draft'),
                    new ToolbarAction(
                        'sulu_admin.set_unpublished'),
                ]
            );
        }

        $viewCollection->add(
            $this->viewBuilderFactory->createPreviewFormViewBuilder(static::NEWS_EDIT_FORM_VIEW.'.details', '/details')
                ->setResourceKey(Article::RESOURCE_KEY)
                ->setFormKey(self::NEWS_FORM_KEY_EDIT)
                ->setTabTitle('sulu_admin.details')
                ->addToolbarActions($formToolbarActions)
                ->setParent(static::NEWS_EDIT_FORM_VIEW)
        );

        $viewCollection->add(
            $this->viewBuilderFactory
                ->createPreviewFormViewBuilder(static::NEWS_EDIT_FORM_VIEW.'.details_excerpt', '/excerpt')
                ->disablePreviewWebspaceChooser()
                ->setResourceKey(Article::RESOURCE_KEY)
                ->setFormKey('article_excerpt')
                ->setTabTitle('sulu_page.excerpt')
                ->addToolbarActions($formToolbarActions)
                ->setTitleVisible(true)
                ->setParent(static::NEWS_EDIT_FORM_VIEW)
        );

        $viewCollection->add(
            $this->viewBuilderFactory->createPreviewFormViewBuilder(static::NEWS_EDIT_FORM_VIEW.'.details_settings', '/details-settings')
                ->setResourceKey(Article::RESOURCE_KEY)
                ->setFormKey(self::NEWS_FORM_KEY_SETTINGS)
                ->setTabTitle('sulu_admin.settings')
                ->addToolbarActions($formToolbarActions)
                ->setParent(static::NEWS_EDIT_FORM_VIEW)
        );
        if ($this->activityViewBuilderFactory->hasActivityListPermission()) {
            $viewCollection->add(
                $this->activityViewBuilderFactory
                    ->createActivityListViewBuilder(
                        static::NEWS_EDIT_FORM_VIEW.'.activity',
                        '/activity',
                        Article::RESOURCE_KEY
                    )
                    ->setParent(static::NEWS_EDIT_FORM_VIEW)
            );
        }

        $viewCollection->add(
            $this->viewBuilderFactory
                ->createPreviewFormViewBuilder(static::NEWS_EDIT_FORM_VIEW.'.details_seo', '/seo')
                ->disablePreviewWebspaceChooser()
                ->setResourceKey(Article::RESOURCE_KEY)
                ->setFormKey('article_seo')
                ->setTabTitle('sulu_page.seo')
                ->addToolbarActions($formToolbarActions)
                ->setTitleVisible(true)
                ->setTabOrder(2048)
                ->setParent(static::NEWS_EDIT_FORM_VIEW)
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getSecurityContexts()
    {
        return [
            'Sulu' => [
                'Article' => [
                    static::SECURITY_CONTEXT => [
                        PermissionTypes::VIEW,
                        PermissionTypes::ADD,
                        PermissionTypes::EDIT,
                        PermissionTypes::DELETE,
                        PermissionTypes::LIVE,
                        PermissionTypes::SECURITY,
                    ],
                ],
            ],
        ];
    }

    public function getConfigKey(): ?string
    {
        return 'sulu_article';
    }
}
