<h1 align="center">SuluArticleBundle</h1>
<p align="center">
    <a href="https://bitbucket.org/digital_web_creative_agency/sulu-article-bundle/src/master/LICENSE" target="_blank">
        <img src="https://img.shields.io/packagist/l/digital-web/sulu-article-bundle?style=flat-square" alt="GitHub license">
    </a>
    <a href="https://packagist.org/packages/digital-web/sulu-article-bundle" target="_blank">
        <img src="https://img.shields.io/packagist/v/digital-web/sulu-article-bundle?color=green&label=Version&style=flat-square" alt="Packagist release">
    </a>
    <a href="https://bitbucket.org/digital_web_creative_agency/sulu-article-bundle/pull-requests" target="_blank">
        <img src="https://img.shields.io/bitbucket/pr/digital_web_creative_agency/sulu-article-bundle?style=flat-square" alt="Pull requests">
    </a>    
    <a href="https://bitbucket.org/digital_web_creative_agency/sulu-article-bundle/releases" target="_blank">
        <img src="https://img.shields.io/badge/sulu%20compatibility-%3E=2.5-52b6ca.svg" alt="Sulu compatibility">
    </a>    
</p>


[![ezgif-5-d1dd7235da05.gif](https://i.postimg.cc/fTt3nZkh/ezgif-5-d1dd7235da05.gif)](https://postimg.cc/tYbRWKhr)


## Requirements

* PHP 8.0
* Sulu ^2.5.*
* Symfony ^5.0 || ^6.0

## Features
* List view of Article
* Routing
* Preview
* SULU Media include
* Content Blocks (Title,Editor,Image,Quote)
* Activity Log
* SEO


## Installation

### Install the bundle 

Execute the following [composer](https://getcomposer.org/) command to add the bundle to the dependencies of your 
project:

```bash

composer require digitalweb/sulu-article-bundle --with-all-dependencies

```
Afterwards, visit the [bundle documentation](Resources/docs) to find out how to set up and configure the SuluArticleBundle to your specific needs.
