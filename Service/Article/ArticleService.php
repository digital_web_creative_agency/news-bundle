<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluArticleBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluArticleBundle\Service\Article;

use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Sulu\Bundle\ActivityBundle\Application\Collector\DomainEventCollectorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use DigitalWeb\Bundle\SuluArticleBundle\Entity\Factory\ArticleFactory;
use DigitalWeb\Bundle\SuluArticleBundle\Entity\Factory\ArticleRouteFactory;
use DigitalWeb\Bundle\SuluArticleBundle\Entity\Article;
use DigitalWeb\Bundle\SuluArticleBundle\Event\ArticleCreatedActivityEvent;
use DigitalWeb\Bundle\SuluArticleBundle\Event\ArticleModifiedActivityEvent;
use DigitalWeb\Bundle\SuluArticleBundle\Event\ArticleRemovedActivityEvent;
use DigitalWeb\Bundle\SuluArticleBundle\Repository\ArticleRepository;
// use Sulu\Component\DocumentManager\DocumentManagerInterface;

class ArticleService implements ArticleServiceInterface
{
    /**
     * @var ArticleRepository
     */
    private $articleRepository;

    /**
     * @var ArticleFactory
     */
    private $articleFactory;

    /**
     * @var object|string
     */
    private $loginUser;

    /**
     * @var ArticleRouteFactory
     */
    private $routeFactory;

    /**
     * @var DomainEventCollectorInterface
     */
    private $domainEventCollector;

    /**
     * @var DocumentManagerInterface
     */
    private $documentManager;

    /**
     * ArticleService constructor.
     */
    public function __construct(
        ArticleRepository $articleRepository,
        ArticleFactory $articleFactory,
        ArticleRouteFactory $routeFactory,
        // DocumentManagerInterface $documentManager,
        TokenStorageInterface $tokenStorage,
        DomainEventCollectorInterface $domainEventCollector
    ) {
        $this->articleRepository = $articleRepository;
        $this->articleFactory = $articleFactory;
        $this->routeFactory = $routeFactory;
        // $this->documentManager = $documentManager;
        $this->domainEventCollector = $domainEventCollector;

        if ($tokenStorage->getToken()) {
            $this->loginUser = $tokenStorage->getToken()->getUser();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function saveNewArticle(array $data, string $locale): Article
    {
        try {
            $article = $this->articleFactory->generateArticleFromRequest(new Article(), $data, $locale);
        } catch (\Exception $e) {
        }

        /** @var Article $article */
        if (!$article->getCreator()) {
            $article->setCreator($this->loginUser->getContact());
        }
        $article->setchanger($this->loginUser->getContact());

        $this->articleRepository->save($article);

        $this->routeFactory->generateArticleRoute($article);

        $this->domainEventCollector->collect(new ArticleCreatedActivityEvent($article, ['name' => $article->getTitle()]));
        $this->articleRepository->save($article);
        $this->documentManager->publish($article, $locale);

        return $article;
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function updateArticle($data, Article $article, string $locale): Article
    {
        try {
            $article = $this->articleFactory->generateArticleFromRequest($article, [], $locale, false);
        } catch (\Exception $e) {
        }

        $article->setchanger($this->loginUser->getContact());

        if ($article->getRoute()->getPath() !== $data['route']) {
            $route = $this->routeFactory->updateArticleRoute($article, $data['route']);
            $article->setRoute($route);
        }

        $this->domainEventCollector->collect(new ArticleModifiedActivityEvent($article, ['name' => $article->getTitle()]));
        $this->articleRepository->save($article);

        return $article;
    }

    public function updateArticlePublish(Article $article, array $data): Article
    {
        switch ($data['action']) {
            case 'enable':
                $article = $this->articleFactory->generateArticleFromRequest($article, [], null, true);

                break;

            case 'disable':
                $article = $this->articleFactory->generateArticleFromRequest($article, [], null, false);

                break;
        }
        $this->domainEventCollector->collect(new ArticleModifiedActivityEvent($article, ['name' => $article->getTitle()]));
        $this->articleRepository->save($article);

        return $article;
    }

    public function removeArticle(int $id): void
    {
        $article = $this->articleRepository->findById($id);
        if (!$article) {
            throw new \Exception($id);
        }

        $this->domainEventCollector->collect(new ArticleRemovedActivityEvent($article, ['name' => $article->getTitle()]));
        $this->articleRepository->remove($id);
    }
}
