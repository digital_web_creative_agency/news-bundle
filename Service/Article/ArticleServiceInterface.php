<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluArticleBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluArticleBundle\Service\Article;

use DigitalWeb\Bundle\SuluArticleBundle\Entity\Article;

interface ArticleServiceInterface
{
    public function saveNewArticle(array $data, string $locale): Article;

    public function updateArticle($data, Article $article, string $locale): Article;
}
