## Installation

### Install the bundle 

Execute the following [composer](https://getcomposer.org/) command to add the bundle to the dependencies of your 
project:

```bash

composer require digital-web/sulu-article-bundle

```

### Enable the bundle 
 
 Enable the bundle by adding it to the list of registered bundles in the `config/bundles.php` file of your project:
 
 ```php
 return [
     /* ... */
     DigitalWeb\Bundle\SuluArticleBundle\ArticleBundle::class => ['all' => true],
 ];
 ```

### Update schema
```shell script
bin/console doctrine:schema:update --force
```

## Bundle Config
    
Define the Admin Api Route in `routes_admin.yaml`
```yaml
sulu_article.admin:
  type: rest
  resource: sulu_article.rest.controller
  prefix: /admin/api
  name_prefix: app.
```

## Template
After the installation, a article [Template](template.md) must be set up for the frontend.
