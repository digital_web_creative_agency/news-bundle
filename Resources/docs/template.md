### Twig-Extensions

If the bundle default controller is used. A template must be created in `article/index.html.twig`.

#### Example template
 ```php
{% block content %}
<h2>{{ article.title }}</h2>

    {% set header = sulu_resolve_media(article.header.id, 'de') %}
    <img src="{{ header.thumbnails['sulu-260x'] }}" alt="{{ header.title }}" title="{{ header.title }}" />

    <p>{{ article.teaser }}</p>
    
    {% for contentItem in article.content  %}
        {% if contentItem.type == 'editor'  %}
            <p>{{ contentItem.text | raw }}</p>
        {% endif %}
    {% endfor %}
{% endblock %}
 ```
