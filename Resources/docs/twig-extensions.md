### Twig-Extensions

#### sulu_resolve_article

Returns article for given id.

 ```php
{% set article = sulu_resolve_article('1') %}
{{ article.title }}
 ```

Arguments:

    id: int - The id of requested article.

Returns:

    object - Object with all needed properties, like title
