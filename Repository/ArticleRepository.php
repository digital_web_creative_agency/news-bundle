<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluArticleBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluArticleBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Sulu\Component\SmartContent\Orm\DataProviderRepositoryInterface;
use DigitalWeb\Bundle\SuluArticleBundle\Entity\Article;

/**
 * Class ArticleRepository.
 */
class ArticleRepository extends EntityRepository implements DataProviderRepositoryInterface
{
    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function save(Article $article): void
    {
        $this->getEntityManager()->persist($article);
        $this->getEntityManager()->flush();
    }

    public function getPublishedArticle(): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('n')
            ->from('ArticleBundle:Article', 'n')
            ->where('n.enabled = 1')
            ->andWhere('n.publishedAt <= :created')
            ->setParameter('created', date('Y-m-d H:i:s'))
            ->orderBy('n.publishedAt', 'DESC')
        ;

        $article = $qb->getQuery()->getResult();

        if (!$article) {
            return [];
        }

        return $article;
    }

    public function findById(int $id): ?Article
    {
        $article = $this->find($id);
        if (!$article) {
            return null;
        }

        return $article;
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(int $id): void
    {
        $this->getEntityManager()->remove(
            $this->getEntityManager()->getReference(
                $this->getClassName(),
                $id
            )
        );
        $this->getEntityManager()->flush();
    }

    public function findByFilters($filters, $page, $pageSize, $limit, $locale, $options = [])
    {
        return $this->getPublishedArticle();
    }
}
