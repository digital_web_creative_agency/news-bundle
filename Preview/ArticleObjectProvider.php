<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluArticleBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluArticleBundle\Preview;

use Sulu\Bundle\PreviewBundle\Preview\Object\PreviewObjectProviderInterface;
use DigitalWeb\Bundle\SuluArticleBundle\Admin\ArticleAdmin;
use DigitalWeb\Bundle\SuluArticleBundle\Entity\Article;
use DigitalWeb\Bundle\SuluArticleBundle\Repository\ArticleRepository;

class ArticleObjectProvider implements PreviewObjectProviderInterface
{
    /**
     * @var ArticleRepository
     */
    private $articleRepository;

    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    public function getObject($id, $locale): ?Article
    {
        return $this->articleRepository->findById((int) $id);
    }

    public function getId($object): string
    {
        return $object->getId();
    }

    public function setValues($object, $locale, array $data): void
    {
        // TODO: Implement setValues() method.
    }

    public function setContext($object, $locale, array $context): Article
    {
        if (\array_key_exists('template', $context)) {
            $object->setStructureType($context['template']);
        }

        return $object;
    }

    public function serialize($object): string
    {
        return serialize($object);
    }

    public function deserialize($serializedObject, $objectClass): Article
    {
        return unserialize($serializedObject);
    }

    public function getSecurityContext($id, $locale): ?string
    {
        return ArticleAdmin::SECURITY_CONTEXT;
    }
}
