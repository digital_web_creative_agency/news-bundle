<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluArticleBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluArticleBundle\Controller\Admin;

use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandlerInterface;
use Sulu\Component\Rest\AbstractRestController;
use Sulu\Component\Rest\Exception\EntityNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use DigitalWeb\Bundle\SuluArticleBundle\Admin\DoctrineListRepresentationFactory;
use DigitalWeb\Bundle\SuluArticleBundle\Api\Article as ArticleApi;
use DigitalWeb\Bundle\SuluArticleBundle\Entity\Article;
use DigitalWeb\Bundle\SuluArticleBundle\Repository\ArticleRepository;
use DigitalWeb\Bundle\SuluArticleBundle\Service\Article\ArticleService;

class ArticleController extends AbstractRestController implements ClassResourceInterface
{
    // serialization groups for contact
    protected static $oneArticleSerializationGroups = [
        'partialMedia',
        'fullArticle',
    ];

    /**
     * @var ArticleRepository
     */
    private $repository;

    /**
     * @var ArticleService
     */
    private $articleService;

    /**
     * @var DoctrineListRepresentationFactory
     */
    private $doctrineListRepresentationFactory;

    /**
     * ArticleController constructor.
     */
    public function __construct(
        ViewHandlerInterface $viewHandler,
        TokenStorageInterface $tokenStorage,
        ArticleRepository $repository,
        ArticleService $articleService,
        DoctrineListRepresentationFactory $doctrineListRepresentationFactory,
    ) {
        parent::__construct($viewHandler, $tokenStorage);

        $this->repository = $repository;
        $this->articleService = $articleService;
        $this->doctrineListRepresentationFactory = $doctrineListRepresentationFactory;
    }

    public function cgetAction(Request $request): Response
    {
        $locale = $request->query->get('locale');
        $listRepresentation = $this->doctrineListRepresentationFactory->createDoctrineListRepresentation(
            Article::RESOURCE_KEY,
            [],
            ['locale' => $locale]
        );

        return $this->handleView($this->view($listRepresentation));
    }

    public function getAction(int $id, Request $request): Response
    {
        if (!$entity = $this->repository->findById($id)) {
            throw new NotFoundHttpException();
        }

        $apiEntity = $this->generateApiArticleEntity($entity, $this->getLocale($request));

        $view = $this->generateViewContent($apiEntity);

        return $this->handleView($view);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function postAction(Request $request): Response
    {
        $article = $this->articleService->saveNewArticle($request->request->all(), $this->getLocale($request));

        $apiEntity = $this->generateApiArticleEntity($article, $this->getLocale($request));

        $view = $this->generateViewContent($apiEntity);

        return $this->handleView($view);
    }

    /**
     * @Rest\Post("/article/{id}")
     */
    public function postTriggerAction(int $id, Request $request): Response
    {
        $article = $this->repository->findById($id);
        if (!$article) {
            throw new NotFoundHttpException();
        }
        
        $article = $this->articleService->updateArticlePublish($article, $request->query->all());
        $apiEntity = $this->generateApiArticleEntity($article, $this->getLocale($request));
        $view = $this->generateViewContent($apiEntity);

        return $this->handleView($view);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function putAction(int $id, Request $request): Response
    {
        $entity = $this->repository->findById($id);
        if (!$entity) {
            throw new NotFoundHttpException();
        }

        $updatedEntity = $this->articleService->updateArticle($request->request->all(), $entity, $this->getLocale($request));
        $apiEntity = $this->generateApiArticleEntity($updatedEntity, $this->getLocale($request));
        $view = $this->generateViewContent($apiEntity);

        return $this->handleView($view);
    }

    /**
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteAction(int $id): Response
    {
        try {
            $this->articleService->removeArticle($id);
        } catch (\Exception $tnfe) {
            throw new EntityNotFoundException(self::$entityName, $id);
        }

        return $this->handleView($this->view());
    }

    public static function getPriority(): int
    {
        return 0;
    }

    protected function generateApiArticleEntity(Article $entity, string $locale): ArticleApi
    {
        return new ArticleApi($entity, $locale);
    }

    protected function generateViewContent(ArticleApi $entity): View
    {
        $view = $this->view($entity);
        $context = new Context();
        $context->setGroups(static::$oneArticleSerializationGroups);

        return $view->setContext($context);
    }
}
