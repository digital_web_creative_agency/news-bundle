<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluArticleBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluArticleBundle\Controller;

use Sulu\Bundle\PreviewBundle\Preview\Preview;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use DigitalWeb\Bundle\SuluArticleBundle\Entity\Article;

/**
 * Class ArticleWebsiteController.
 */
class ArticleWebsiteController extends AbstractController
{
    public function indexAction(Article $article, $attributes = [], $preview = false, $partial = false): Response
    {
        if (!$article) {
            throw new NotFoundHttpException();
        }

        if ($partial) {
            $content = $this->renderBlock(
                '@Article/index.html.twig',
                'content',
                ['article' => $article]
            );
        } elseif ($preview) {
            $content = $this->renderPreview(
                '@Article/index.html.twig',
                ['article' => $article]
            );
        } else {
            $content = $this->renderView(
                '@Article/index.html.twig',
                ['article' => $article]
            );
        }

        return new Response($content);
    }

    protected function renderPreview(string $view, array $parameters = []): string
    {
        $parameters['previewParentTemplate'] = $view;
        $parameters['previewContentReplacer'] = Preview::CONTENT_REPLACER;

        return $this->renderView('@SuluWebsite/Preview/preview.html.twig', $parameters);
    }

    /**
     * Returns rendered part of template specified by block.
     *
     * @param mixed $template
     * @param mixed $block
     * @param mixed $attributes
     */
    protected function renderBlock($template, $block, $attributes = [])
    {
        $twig = $this->container->get('twig');
        $attributes = $twig->mergeGlobals($attributes);

        $template = $twig->load($template);

        $level = ob_get_level();
        ob_start();

        try {
            $rendered = $template->renderBlock($block, $attributes);
            ob_end_clean();

            return $rendered;
        } catch (\Exception $e) {
            while (ob_get_level() > $level) {
                ob_end_clean();
            }

            throw $e;
        }
    }
}
