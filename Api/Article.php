<?php

declare(strict_types=1);

/*
 * This file is part of DigitalWeb/SuluArticleBundle.
 *
 * (c) Digital web
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace DigitalWeb\Bundle\SuluArticleBundle\Api;

use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\VirtualProperty;
use Sulu\Component\Rest\ApiWrapper;
use DigitalWeb\Bundle\SuluArticleBundle\Entity\Article as ArticleEntity;

/**
 * The Article class which will be exported to the API.
 *
 * @ExclusionPolicy("all")
 */
class Article extends ApiWrapper
{
    public function __construct(ArticleEntity $contact, $locale)
    {
        // @var ArticleEntity entity
        $this->entity = $contact;
        $this->locale = $locale;
    }

    /**
     * Get id.
     *
     * @VirtualProperty
     *
     * @SerializedName("id")
     * @Groups({"fullArticle"})
     */
    public function getId(): ?int
    {
        return $this->entity->getId();
    }

    /**
     * @VirtualProperty
     *
     * @SerializedName("title")
     * @Groups({"fullArticle"})
     */
    public function getTitle()
    {
        return $this->entity->getTitle();
    }

    /**
     * @VirtualProperty
     *
     * @SerializedName("teaser")
     * @Groups({"fullArticle"})
     */
    public function getTeaser()
    {
        return $this->entity->getTeaser();
    }

    /**
     * @VirtualProperty
     *
     * @SerializedName("content")
     * @Groups({"fullArticle"})
     */
    public function getContent(): array
    {
        if (!$this->entity->getContent()) {
            return [];
        }

        return $this->entity->getContent();
    }

    /**
     * @VirtualProperty
     *
     * @SerializedName("enabled")
     * @Groups({"fullArticle"})
     */
    public function isEnabled(): bool
    {
        return true;
    }

    /**
     * @VirtualProperty
     *
     * @SerializedName("publishedAt")
     * @Groups({"fullArticle"})
     */
    public function getPublishedAt()
    {
        return $this->entity->getPublishedAt();
    }

    /**
     * @VirtualProperty
     *
     * @SerializedName("route")
     * @Groups({"fullArticle"})
     */
    public function getRoute()
    {
        if ($this->entity->getRoute()) {
            return $this->entity->getRoute()->getPath();
        }

        return '';
    }

    /**
     * Get tags.
     *
     * @VirtualProperty
     *
     * @SerializedName("tags")
     * @Groups({"fullArticle"})
     */
    public function getTags(): array
    {
        return $this->entity->getTagNameArray();
    }

    /**
     * Get the contacts avatar and return the array of different formats.
     *
     * @VirtualProperty
     *
     * @SerializedName("header")
     * @Groups({"fullArticle"})
     */
    public function getHeader(): array
    {
        if ($this->entity->getHeader()) {
            return [
                'id' => $this->entity->getHeader()->getId(),
            ];
        }

        return [];
    }

    /**
     * Get tags.
     *
     * @VirtualProperty
     *
     * @SerializedName("authored")
     * @Groups({"fullArticle"})
     */
    public function getAuthored(): \DateTime
    {
        return $this->entity->getCreated();
    }

    /**
     * Get tags.
     *
     * @VirtualProperty
     *
     * @SerializedName("created")
     * @Groups({"fullArticle"})
     */
    public function getCreated(): \DateTime
    {
        return $this->entity->getCreated();
    }

    /**
     * Get tags.
     *
     * @VirtualProperty
     *
     * @SerializedName("changed")
     * @Groups({"fullArticle"})
     */
    public function getChanged(): \DateTime
    {
        return $this->entity->getChanged();
    }

    /**
     * Get tags.
     *
     * @VirtualProperty
     *
     * @SerializedName("author")
     * @Groups({"fullArticle"})
     */
    public function getAuthor(): int
    {
        return $this->entity->getCreator()->getId();
    }

    /**
     * Get tags.
     *
     * @VirtualProperty
     *
     * @SerializedName("ext")
     * @Groups({"fullArticle"})
     */
    public function getSeo()
    {
        $seo = ['seo'];
        $seo['seo'] = $this->getEntity()->getSeo();

        return $seo;
    }
}
